----------------------------------------------------------------------------------
-- Module Name: full_adder_logic_4bits
-- Description: A full adder only with logic element for two 4 bits
--	numbers
-- Revision: V1.0 -- 24.04.24.09.54
-- Additional Comments: No
----------------------------------------------------------------------------------

-- Libraries used
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Creation of an entity
entity full_adder_logic_4bits is
    Port ( cin : in STD_LOGIC;
           a : in STD_LOGIC_VECTOR (3 downto 0);	-- input a
           b : in STD_LOGIC_VECTOR (3 downto 0);	-- input b
           s : out STD_LOGIC_VECTOR (3 downto 0);	-- output s
           cout : out STD_LOGIC);					-- carry out
end full_adder_logic_4bits;

--Creation of an architecture
architecture Behavioral of full_adder_logic_4bits is

    signal cout0,cout1,cout2 : std_logic;

begin

    s(0) <= a(0) xor b(0) xor cin;
    cout0 <= (a(0) and b(0)) or (a(0) and cin) or (cin and b(0));

    s(1) <= a(1) xor b(1) xor cout0;
    cout1 <= (a(1) and b(1)) or (a(1) and cout0) or (cout0 and b(1));

    s(2) <= a(2) xor b(2) xor cout1;
    cout2 <= (a(2) and b(2)) or (a(2) and cout1) or (cout1 and b(2));

    s(3) <= a(3) xor b(3) xor cout2;
    cout <= (a(3) and b(3)) or (a(3) and cout2) or (cout2 and b(3));

end Behavioral;
