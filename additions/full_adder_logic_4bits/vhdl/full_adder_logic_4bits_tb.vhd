----------------------------------------------------------------------------------
-- Module Name: full_adder_logic_4bits_tb
-- Description: Test bench file of a full adder only with logic element
--	for two 4 bits numbers
-- Revision: V1.0 -- 24.04.24.09.54
-- Additional Comments: No
----------------------------------------------------------------------------------

-- Libraries used
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


-- Creation of an entity
entity full_adder_logic_4bits_tb is
end full_adder_logic_4bits_tb;

architecture Behavioral of full_adder_logic_4bits_tb is

    component full_adder_logic_4bits is port
            (
            cin : in STD_LOGIC;
            a : in STD_LOGIC_VECTOR (3 downto 0);
            b : in STD_LOGIC_VECTOR (3 downto 0);
            s : out STD_LOGIC_VECTOR (3 downto 0);
            cout : out STD_LOGIC
            );
    end component;
    
    signal cin, cout : std_logic;
    signal a,b,s : std_logic_vector(3 downto 0);

begin

    uut : full_adder_logic_4bits port map
    (
        cin => cin,
        cout => cout,
        a => a,
        b => b,
        s => s
    );

    process
    begin
        cin <= '0'; a <= "1101"; b <= "0110";
        wait for 100ns;
        cin <= '1'; a <= "0001"; b <= "0110";
        wait for 100ns;
    end process;

end Behavioral;
